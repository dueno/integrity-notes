/* SPDX-License-Identifier: LGPL-2.1-or-later */

.section ".note.integrity", "a"
	.p2align 2
	.long 1f-0f
	.long 3f-2f
	.long 0xcafe2a8e
0:
	.asciz "FDO"
1:
	.p2align 2
2:
	/* Add a placeholder hash value */
	.byte 0,0,0,0,0,0,0,0
	.byte 0,0,0,0,0,0,0,0
	.byte 0,0,0,0,0,0,0,0
	.byte 0,0,0,0,0,0,0,0
3:
	.p2align 2
