#include "integrity.h"

#include <stdio.h>

const char *
greeting (void)
{
  if (!check_integrity ("./libhello.so"))
    {
      fprintf (stderr, "integrity check failed\n");
      return NULL;
    }

  return "Hello world!";
}
