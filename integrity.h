/* SPDX-License-Identifier: LGPL-2.1-or-later */

#include <stdbool.h>
#include <stdint.h>

#define FIPS_NOTE_NAME "FDO"
#define NT_FIPS_INTEGRITY 0xcafe2a8e

/* Calculate integrity of the shared library FILE, loaded as NAME. */
int calculate_integrity (const char *file, const char *name, uint8_t digest[32]);

/* Check integrity of the shared library FILE. */
bool check_integrity (const char *file);
