# integrity-notes

This is another attempt to embed the integrity information of a
library into the library itself.

The main differences to the other approaches are:

- The library integrity is calculated only once at build time, not at
  packaging time (i.e., after `strip`). This is done by calculating integrity
  over segments pointed by ELF program headers, not over the entire file.

- This uses a dedicated ELF note section (`.note.integrity`), so it
  can be safely (?) overwritten.

## Usage

To compile:

```console
$ make
gcc -fPIC -o libhello.so -shared libhello.c integrity.c integrity-note.s -I/usr/include/p11-kit-1  -lgnutls
gcc -o hello hello.c libhello.so
gcc -fPIC -o generate-integrity-note integrity.h integrity.c generate-integrity-note.c -I/usr/include/p11-kit-1  -lgnutls  -ldl
```

At this point the linked shared library `libhello.so` doesn't have integrity information, so running the `hello` program will fail:

```console
$ LD_LIBRARY_PATH=. ./hello
integrity check failed
```

To embed integrity information:

```console
$ make stamp
./generate-integrity-note ./libhello.so > t && \
objcopy --update-section .note.integrity=t libhello.so && \
rm -f t && \
touch stamp
```

Re-run `hello` again and now it succeeds:
```console
$ LD_LIBRARY_PATH=. ./hello
Hello world!
```

Even after strip:
```console
$ strip libhello.so
$ LD_LIBRARY_PATH=. ./hello
Hello world!
```

But not after modifying the .text:
```console
$ sed -i 's/world/w0rld/' libhello.so
$ LD_LIBRARY_PATH=. ./hello
integrity check failed
```
