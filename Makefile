lib_sources = libhello.c integrity.c integrity-note.s

GNUTLS_CFLAGS = $(shell pkg-config gnutls --cflags)
GNUTLS_LIBS = $(shell pkg-config gnutls --libs)

.PHONY: all
all: hello generate-integrity-note

libhello.so: $(lib_sources)
	gcc -fPIC -o $@ -shared $(lib_sources) $(GNUTLS_CFLAGS) $(GNUTLS_LIBS) $(CFLAGS) $(LDFLAGS)

hello: hello.c libhello.so
	gcc -o $@ $< libhello.so $(CFLAGS) $(LDFLAGS)

generate-integrity-note: integrity.h integrity.c generate-integrity-note.c
	gcc -fPIC -o $@ $^ $(GNUTLS_CFLAGS) $(GNUTLS_LIBS) -ldl $(CFLAGS) $(LDFLAGS)

stamp: generate-integrity-note libhello.so
	./generate-integrity-note ./libhello.so > t && \
	objcopy --update-section .note.integrity=t libhello.so && \
	rm -f t && \
	touch $@

.PHONY: clean
clean:
	rm -f hello libhello.so generate-integrity-note stamp
