/* SPDX-License-Identifier: LGPL-2.1-or-later */

#include "integrity.h"
#include <dlfcn.h>
#include <errno.h>
#include <error.h>
#include <stdio.h>
#include <stdlib.h>

#define ALIGN_UP(val, align) (((val) + (align) - 1) & ~((align) - 1))

static inline void
write_long_le (FILE *out, uint32_t v)
{
  uint8_t buf[4];

  buf[0] = v;
  buf[1] = v >> 8;
  buf[2] = v >> 16;
  buf[3] = v >> 24;

  fwrite (buf, 1, 4, out);
}

int
main (int argc, char **argv)
{
  if (argc != 2)
    {
      fprintf (stderr, "Usage: generate-integrity-note LIBRARY\n");
      exit (EXIT_FAILURE);
    }

  void *handle = dlopen (argv[1], RTLD_LAZY);
  if (!handle)
    error (EXIT_FAILURE, errno, "dlopen");

  uint8_t digest[32];
  int ret = calculate_integrity (argv[1], argv[1], digest);
  if (ret < 0)
    error (EXIT_FAILURE, 0, "failed to calculate integrity\n");

  write_long_le (stdout, ALIGN_UP(sizeof(FIPS_NOTE_NAME), 4));
  write_long_le (stdout, 32);
  write_long_le (stdout, 0xcafe2a8e);
  fwrite (FIPS_NOTE_NAME, 1, sizeof(FIPS_NOTE_NAME), stdout);
  fwrite (digest, 1, 32, stdout);

  dlclose (handle);

  return EXIT_SUCCESS;
}
